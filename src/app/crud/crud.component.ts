import { Component, OnInit } from '@angular/core';
import { Data } from './data';
import * as _ from 'lodash';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.scss']
})
export class CrudComponent implements OnInit {
  dataObj: Data;
  isEdit: boolean;
  filterdList = [];
  searchQuery: string;
  constructor() { }

  ngOnInit() {
    this.dataObj = new Data();
    this.filterdList = _.cloneDeep(this.data);
  }

  data = [
    { id: 1, name: "Raman", mobileNo: 8607722596 },
    { id: 2, name: "Ram", mobileNo: 9812150518 },
    { id: 3, name: "Mohit", mobileNo: 9812545689 },
  ]

  save() {
    if (this.isEdit) {
      this.edit(this.dataObj);
      if(this.dataObj = new Data()){
        this.isEdit=false;
      }
      
    }
    else {
      this.add();
      this.filterdList = _.cloneDeep(this.data);
    }
  }

  search() {
    this.data = _.cloneDeep(this.filterdList);
    this.data = this.filterdList.filter(ele =>
      ele.name.toLowerCase().includes(this.searchQuery.toLowerCase()))
  }

  add() {
    this.data.push(this.dataObj);

  }
  edit(obj) {
    this.isEdit = true;
    this.dataObj = obj;
  }

  delete(id) {
    this.data.splice(id, 1);
  }
}
